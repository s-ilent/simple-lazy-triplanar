# Simple Lazy Triplanar

# [Can't find the download link? Click here!](https://gitlab.com/s-ilent/simple-lazy-triplanar/-/archive/master/simple-lazy-triplanar-master.zip)

A Standard-based triplanar mapping (applying textures without UV coordinates) shader for Unity. Contains a mode that adds stochastic (random) sampling to break up repetition in textures.

![Preview](https://gitlab.com/s-ilent/simple-lazy-triplanar/-/wikis/uploads/4bec6176e8b6b9ece1e886b2239076ea/Unity_2020-05-19_14-16-31.pngc.png.jpg)

## Installation

Download the repository. Then place the Shader/ folder with the shader into your Assets/ directory.

## Usage

You can use this shader on any material and it'll apply the albedo and normal texture chosen in world space. However, if you want to use other features of Standard, you'll need to pack them into a MOES map. You can use the included SLT Texture Combiner to merge different maps (Metalness, Occlusion, Emission, and Smoothness) into one map. Open it from the Window menu in Unity.

## License?

MIT license.